# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.

ZSH_THEME="fletcherm"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export PS1="%{$fg_no_bold[cyan]%}%n%{$fg_no_bold[magenta]%}•%{$fg_no_bold[green]%}%2~$(git_prompt_info)%{$reset_color%}» "

export PATH=/opt/local/bin/:/opt/local/sbin/:$PATH

#### my alias
export HOME_PATH="/Users/jhlee"

alias "cd.."="cd .." 
alias "cd...."="cd ../.." 

sublime() {
	echo "# [  Sublime Text 3 > $1  ]";
	/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl "$1"
}

alias "subl"="sublime"
alias "sb"="sublime"

alias ssh.plasse="ssh plasse.hanyang.ac.kr -p 2200"
alias ftp.hostinger="ftp u910976883@31.170.165.250"

ws() { 
	cd $HOME_PATH"/workspace/""$1"
}

alias ocaml="rlwrap ocaml"


silent() {
	"$1" 1>/dev/null 2>/dev/null
}

alias "sshme"="ssh jhlee@167.88.37.189"
alias "ftp_transmission"="ftp transmission@167.88.37.189 7020"
alias "ftp_hostinger"="ftp u910976883.jhlee@ftp.jhlee.zz.mu"
alias "p3"="python3"

# OPAM configuration
. /Users/jhlee/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# virtualenv
. /usr/local/bin/virtualenvwrapper.sh
alias mkvenv="python3 -m venv "
venv() {
    source $1/bin/activate
}

alias "dot_open"="cat > /tmp/graph.dot;open /tmp/graph.dot"
