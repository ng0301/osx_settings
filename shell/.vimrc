set nu
set guifont=Source\ Code\ Pro\ for\ Powerline:h14
set hlsearch
set ai
set wmnu
set ruler
set title  
set background=dark
set smartindent


au BufReadPost *
\ if line("'\"") > 0 && line("'\"") <= line("$") |
\ exe "norm g`\"" |
\ endif


if has("syntax")
	 syntax on
	 endif



filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab


colorscheme darkmate

